using estados;
using Microsoft.EntityFrameworkCore;

namespace estados.teste

{
    public class ContextBd_test
    {
        internal static EstadosDbContext CriarBancoContex(string nome)
        {
            var options = new DbContextOptionsBuilder<EstadosDbContext>().
            UseInMemoryDatabase(nome).Options;
            return new EstadosDbContext(options);
        }

        internal static void Add_estados(EstadosDbContext context)
        {
            context.Estados.Add(new Estados()
            {
                uf = "AC",
                nome = "acre"

            });
            context.SaveChanges();
        }
    }
}