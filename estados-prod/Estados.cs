using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace estados
{
    [Table("estados")]
    public class Estados
    {
        public int id { get; set; }

        public string uf { get; set; }

        public string nome { get; set; }
    }
}