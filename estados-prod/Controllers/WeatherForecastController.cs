﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace estados.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private EstadosDbContext _context;

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(EstadosDbContext context)
        {
           
            _context = context;
        }

        

        [HttpGet]
        public IImmutableList<Estados> EstadoGet()
        {
            return _context.Estados.ToImmutableList();
        }
    }

    
}
