using Microsoft.EntityFrameworkCore;

namespace estados
{
    public class EstadosDbContext : DbContext
    {
        public DbSet<Estados> Estados { get; set; }

        public EstadosDbContext(DbContextOptions options) : base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           modelBuilder.Entity<Estados>().HasData(
            
            new Estados() { id =1, nome = " Acre ",uf = "AC "},
            new Estados() { id =2, nome = " Alagoas ",uf = "AL "},
            new Estados() { id =3, nome = " Amapá ",uf = "AP "},
            new Estados() { id =4, nome = " Amazonas ",uf = "AM "},
            new Estados() { id =5, nome = " Bahia ",uf = "BA "},
            new Estados() { id =6, nome = " Ceará ",uf = "CE "},
            new Estados() { id =7, nome = " Distrito Federal ",uf = "DF "},
            new Estados() { id =8, nome = " Espírito Santo ",uf = "ES "},
            new Estados() { id =9, nome = " Goiás ",uf = "GO "},
            new Estados() { id =10, nome = " Maranhão ",uf = "MA "},
            new Estados() { id =11, nome = " Mato Grosso ",uf = "MT "},
            new Estados() { id =12, nome = " Mato Grosso do Sul ",uf = "MS "},
            new Estados() { id =13, nome = " Minas Gerais ",uf = "MG "},
            new Estados() { id =14, nome = " Pará ",uf = "PA "},
            new Estados() { id =15, nome = " Paraíba ",uf = "PB "},
            new Estados() { id =16, nome = " Paraná ",uf = "PR "},
            new Estados() { id =17, nome = " Pernambuco ",uf = "PE "},
            new Estados() { id =18, nome = " Piauí ",uf = "PI "},
            new Estados() { id =19, nome = " Rio de Janeiro ",uf = "RJ "},
            new Estados() { id =20, nome = " Rio Grande do Norte ",uf = "RN "},
            new Estados() { id =21, nome = " Rio Grande do Sul ",uf = "RS "},
            new Estados() { id =22, nome = " Rondônia ",uf = "RO "},
            new Estados() { id =23, nome = " Roraima ",uf = "RR "},
            new Estados() { id =24, nome = " Santa Catarina ",uf = "SC "},
            new Estados() { id =25, nome = " São Paulo ",uf = "SP "},
            new Estados() { id =26, nome = " Sergipe ",uf = "SE "},
            new Estados() { id =27, nome = " Tocantins ",uf = "TO "}
            
        );
        }
    }
}