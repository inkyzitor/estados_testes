﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace estados.Migrations
{
    public partial class migrationsinit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "estados",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    uf = table.Column<string>(type: "text", nullable: true),
                    nome = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_estados", x => x.id);
                });

            migrationBuilder.InsertData(
                table: "estados",
                columns: new[] { "id", "nome", "uf" },
                values: new object[,]
                {
                    { 1, " Acre ", "AC " },
                    { 25, " São Paulo ", "SP " },
                    { 24, " Santa Catarina ", "SC " },
                    { 23, " Roraima ", "RR " },
                    { 22, " Rondônia ", "RO " },
                    { 21, " Rio Grande do Sul ", "RS " },
                    { 20, " Rio Grande do Norte ", "RN " },
                    { 19, " Rio de Janeiro ", "RJ " },
                    { 18, " Piauí ", "PI " },
                    { 17, " Pernambuco ", "PE " },
                    { 16, " Paraná ", "PR " },
                    { 15, " Paraíba ", "PB " },
                    { 26, " Sergipe ", "SE " },
                    { 14, " Pará ", "PA " },
                    { 12, " Mato Grosso do Sul ", "MS " },
                    { 11, " Mato Grosso ", "MT " },
                    { 10, " Maranhão ", "MA " },
                    { 9, " Goiás ", "GO " },
                    { 8, " Espírito Santo ", "ES " },
                    { 7, " Distrito Federal ", "DF " },
                    { 6, " Ceará ", "CE " },
                    { 5, " Bahia ", "BA " },
                    { 4, " Amazonas ", "AM " },
                    { 3, " Amapá ", "AP " },
                    { 2, " Alagoas ", "AL " },
                    { 13, " Minas Gerais ", "MG " },
                    { 27, " Tocantins ", "TO " }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "estados");
        }
    }
}
