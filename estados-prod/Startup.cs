using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace estados
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // string connectionString =
            //     "User ID=dev;Password=dev;Host=localhost;Port=5432;Database=Estados;Pooling=true;";
            
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "estados", Version = "v1" });
            });
            Console.WriteLine(StringConexao());
            services.AddDbContext<EstadosDbContext>(options =>
                options.UseNpgsql(StringConexao()));
            
        }

        private string StringConexao()
        {
            var Id = Configuration["Id"];
            var Password = Configuration["Password"];
            var Host = Configuration["Host"];
            var Port = Configuration["Port"];
            var Database = Configuration["Database"];
            var Pooling = Configuration["Pooling"];

            return $"User ID={Id};Password={Password};Host={Host};Port={Port};Database={Database};Pooling={Pooling};";
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "estados v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
    
}
